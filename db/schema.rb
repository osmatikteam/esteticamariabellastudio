# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140922001300) do

  create_table "bar_codes", force: true do |t|
    t.string   "code"
    t.string   "path"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "clients", force: true do |t|
    t.integer  "bar_code_id"
    t.integer  "status_client_id"
    t.integer  "municipality_id"
    t.string   "name"
    t.string   "lastName1"
    t.string   "lastname2"
    t.string   "neibor"
    t.string   "street"
    t.string   "homeNumber"
    t.string   "homeNumberExt"
    t.integer  "zipCode"
    t.string   "birthOn"
    t.string   "celPhoneNumber"
    t.string   "phoneNumber"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "municipalities", force: true do |t|
    t.string   "name"
    t.integer  "state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "states", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "status_clients", force: true do |t|
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "visits", force: true do |t|
    t.integer  "client_id"
    t.string   "employeName"
    t.string   "productsName"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end

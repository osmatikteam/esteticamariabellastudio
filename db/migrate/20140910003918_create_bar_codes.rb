class CreateBarCodes < ActiveRecord::Migration
  def change
    create_table :bar_codes do |t|
      t.string :code
      t.string :path

      t.timestamps
    end
  end
end

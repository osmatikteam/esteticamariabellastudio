class CreateStatusClients < ActiveRecord::Migration
  def change
    create_table :status_clients do |t|
      t.string :description

      t.timestamps
    end
  end
end

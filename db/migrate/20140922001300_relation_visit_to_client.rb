class RelationVisitToClient < ActiveRecord::Migration
  def change
  	 create_table :clients do |t|
      t.belongs_to :bar_code
      t.belongs_to :status_client
      t.belongs_to :municipality
      t.string :name
      t.string :lastName1
      t.string :lastname2
      t.string :neibor
      t.string :street
      t.string :homeNumber
      t.string :homeNumberExt
      t.integer :zipCode
      t.integer :birthOn
      t.string :celPhoneNumber
      t.string :phoneNumber
      t.string :email

      t.timestamps
    end

    create_table :visits do |t|
      t.belongs_to :client
      t.string :employeName
      t.string :productsName

      t.timestamps
    end

    create_table :municipalities do |t|
      t.string :name
      t.integer :state_id

      t.timestamps
  end
end

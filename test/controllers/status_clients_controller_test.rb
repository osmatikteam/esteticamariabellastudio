require 'test_helper'

class StatusClientsControllerTest < ActionController::TestCase
  setup do
    @status_client = status_clients(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:status_clients)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create status_client" do
    assert_difference('StatusClient.count') do
      post :create, status_client: { description: @status_client.description }
    end

    assert_redirected_to status_client_path(assigns(:status_client))
  end

  test "should show status_client" do
    get :show, id: @status_client
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @status_client
    assert_response :success
  end

  test "should update status_client" do
    patch :update, id: @status_client, status_client: { description: @status_client.description }
    assert_redirected_to status_client_path(assigns(:status_client))
  end

  test "should destroy status_client" do
    assert_difference('StatusClient.count', -1) do
      delete :destroy, id: @status_client
    end

    assert_redirected_to status_clients_path
  end
end

require 'test_helper'

class BarCodesControllerTest < ActionController::TestCase
  setup do
    @bar_code = bar_codes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bar_codes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bar_code" do
    assert_difference('BarCode.count') do
      post :create, bar_code: { code: @bar_code.code, path: @bar_code.path }
    end

    assert_redirected_to bar_code_path(assigns(:bar_code))
  end

  test "should show bar_code" do
    get :show, id: @bar_code
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bar_code
    assert_response :success
  end

  test "should update bar_code" do
    patch :update, id: @bar_code, bar_code: { code: @bar_code.code, path: @bar_code.path }
    assert_redirected_to bar_code_path(assigns(:bar_code))
  end

  test "should destroy bar_code" do
    assert_difference('BarCode.count', -1) do
      delete :destroy, id: @bar_code
    end

    assert_redirected_to bar_codes_path
  end
end

class StatusClientsController < ApplicationController
  before_action :set_status_client, only: [:show, :edit, :update, :destroy]

  # GET /status_clients
  # GET /status_clients.json
  def index
    @status_clients = StatusClient.all
  end

  # GET /status_clients/1
  # GET /status_clients/1.json
  def show
  end

  # GET /status_clients/new
  def new
    @status_client = StatusClient.new
  end

  # GET /status_clients/1/edit
  def edit
  end

  # POST /status_clients
  # POST /status_clients.json
  def create
    @status_client = StatusClient.new(status_client_params)

    respond_to do |format|
      if @status_client.save
        format.html { redirect_to @status_client, notice: 'Status client was successfully created.' }
        format.json { render :show, status: :created, location: @status_client }
      else
        format.html { render :new }
        format.json { render json: @status_client.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /status_clients/1
  # PATCH/PUT /status_clients/1.json
  def update
    respond_to do |format|
      if @status_client.update(status_client_params)
        format.html { redirect_to @status_client, notice: 'Status client was successfully updated.' }
        format.json { render :show, status: :ok, location: @status_client }
      else
        format.html { render :edit }
        format.json { render json: @status_client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /status_clients/1
  # DELETE /status_clients/1.json
  def destroy
    @status_client.destroy
    respond_to do |format|
      format.html { redirect_to status_clients_url, notice: 'Status client was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_status_client
      @status_client = StatusClient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def status_client_params
      params.require(:status_client).permit(:description)
    end
end

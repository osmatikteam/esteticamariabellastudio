require 'barby'
require 'barby/barcode/code_128'
require 'barby/barcode/ean_13'
require 'barby/outputter/png_outputter'
require 'barby/outputter/ascii_outputter'
require 'barby/outputter/html_outputter'

class ClientsController < ApplicationController
  before_action :set_client, only: [:show, :edit, :update, :destroy]

  # GET /clients
  # GET /clients.json
  def index
    @clients = Client.all
  end

  # GET /clients/1
  # GET /clients/1.json
  def show
  end

  # GET /clients/new
  def new
    @client = Client.new
    @municipalities = Municipality.all 
    @states = State.all 
  end

  # GET /clients/1/edit
  def edit
  end

  # POST /clients
  # POST /clients.json
  def create
    @code = rand(100000000000..999999999999)
    @path = "BarCodes/Clients/"

    @bar_code = BarCode.new
    @bar_code.code = @code
    @bar_code.path = @path
    @bar_code.save

    @client = Client.new(client_params)
    @client.status_client_id = 1
    @client.bar_code_id = @bar_code.id

    #render :text => client_params

    respond_to do |format|
      if @client.save
        @barcode = Barby::EAN13.new(@code.to_s)
        @barcode_for_html = Barby::HtmlOutputter.new(@barcode)

        barcode = Barby::Code128B.new('The noise of mankind has become too much')
        File.open(@path+@code.to_s+'.png', 'w'){|f|
        f.write barcode.to_png(:height => 20, :margin => 5)
        }


        format.html { redirect_to @client, notice: 'Client was successfully created.' }
        format.json { render :show, status: :created, location: @client }
      else
        format.html { render :new }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /clients/1
  # PATCH/PUT /clients/1.json
  def update
    respond_to do |format|
      if @client.update(client_params)
        format.html { redirect_to @client, notice: 'Client was successfully updated.' }
        format.json { render :show, status: :ok, location: @client }
      else
        format.html { render :edit }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clients/1
  # DELETE /clients/1.json
  def destroy
    @client.status_client_id = 2
    @client.save
    respond_to do |format|
      format.html { redirect_to clients_url, notice: 'Client was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_params
      params.require(:client).permit(:name, :lastName1, :lastname2, :neibor, :street, :homeNumber, :homeNumberExt, :zipCode, :birthOn, :celPhoneNumber, :phoneNumber, :email, :status_client_id, :bar_code_Id, :municipality_id)
    end
end

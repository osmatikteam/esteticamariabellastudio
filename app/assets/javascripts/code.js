/*================================================*/
/*================================================*/
		/* Get current date */
/*================================================*/
/*================================================*/

var meses = new Array ( "Enero",
						"Febrero",
						"Marzo",
						"Abril",
						"Mayo",
						"Junio",
						"Julio",
						"Agosto",
						"Septiembre",
						"Octubre",
						"Noviembre",
						"Diciembre" );

var diaSemana = new Array ( "Domingo",
							 "Lunes",
							 "Martes",
							 "Miércoles",
							 "Jueves",
							 "Viernes",
							 "Sábado" );
var f = new Date();

document.getElementById("Form-timeAddCustomer").innerHTML	=	diaSemana[f.getDay()] +
												", " + f.getDate() +
												" de " + meses[f.getMonth()] +
												" de " + f.getFullYear();
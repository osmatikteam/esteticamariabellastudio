json.array!(@clients) do |client|
  json.extract! client, :id, :name, :lastName1, :lastname2, :neibor, :street, :homeNumber, :homeNumberExt, :zipCode, :birthDay, :birthMonth, :birthYear, :celPhoneNumber, :phoneNumber, :email, :statusId, :barCodeId, :stateId, :municipalityId
  json.url client_url(client, format: :json)
end

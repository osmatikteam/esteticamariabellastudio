json.array!(@bar_codes) do |bar_code|
  json.extract! bar_code, :id, :code, :path
  json.url bar_code_url(bar_code, format: :json)
end

json.array!(@visits) do |visit|
  json.extract! visit, :id, :client_id, :employeName, :productsName
  json.url visit_url(visit, format: :json)
end

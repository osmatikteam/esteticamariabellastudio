json.array!(@status_clients) do |status_client|
  json.extract! status_client, :id, :description
  json.url status_client_url(status_client, format: :json)
end

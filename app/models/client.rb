class Client < ActiveRecord::Base

	has_many :visits

	has_one :bar_code

	has_one :status_client

	has_one :municipality

end
